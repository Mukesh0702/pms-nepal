// import Logout from 'scenes/auth/logout'
import ErrorsPage from "app/modules/ErrorsExamples/ErrorsPage";
//import LoadingHOC from "src/hoc/loadingHOC";
//import AdminLinks from "app/admin/Links/AdminLinks";
import UserLinks from "app/users/Links/UserLinks";
import React, { Component, Suspense } from "react";
// import isEmpty from "~/helpers/isEmpty";
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import { LayoutSplashScreen } from "_metronic/layout";
import { Logout, AuthPage } from "app/modules/Auth";
import AdminLinks from "app/admin/Links/AdminLinks";
import MergedRoutes from "./MergedRoutes";

class Routes extends Component {
  render() {
    const pad = { paddingBottom: 0 };
    return (
      <Switch>
        <Route path="/error" component={ErrorsPage} />
        <Route path="/logout" component={Logout} />
        <MergedRoutes {...this.props} />
      </Switch>
    );
  }
}

export default withRouter(Routes);
