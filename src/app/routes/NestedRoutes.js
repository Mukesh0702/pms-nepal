import React from "react";
import { Switch } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";

export function RouteNest(props) {
    const {isNotPrivate} =props;
    return (
        <Switch>
            <PrivateRoute
                exact={props.exact}
                path={props.path}
                children={props.children}
                component={props.component}
                childRef={props.forwardRef}
                isNotPrivate={props.isNotPrivate}
                {...props}
            />
        </Switch>
        // <Route exact={props.exact} path={props.path} render={ p => <props.component {...p} children={props.children}/> } />
    );
}
