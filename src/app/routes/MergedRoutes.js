import React, { Suspense, lazy } from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute, Layout } from "_metronic/layout";
import { BuilderPage } from "app/pages/BuilderPage";
import { MyPage } from "app/pages/MyPage";
import { DashboardPage } from "app/pages/DashboardPage";
import AdminLinks from "app/admin/Links/AdminLinks";
import UserLinks from "app/users/Links/UserLinks";
import {
  AdminDashboardPath,
  AdminBuilderPath,
  AdminMyPagePath,
} from "app/admin/Links/AdminPaths";
import { UserBuyPath, UserHomePath } from "app/users/Links/UserPaths";
import Buy from "app/users/Scenes/Buy";
import Home from "app/users/Scenes/Home";
import { RouteNest } from "app/routes/NestedRoutes";
import AuthPage from "app/modules/Auth/pages/AuthPage";
import Footer from "app/users/Layouts/Footer";
import Head from "app/users/Layouts/NavBar";
import User from "app/users/Scenes";
import ErrorsPage from "app/modules/ErrorsExamples/ErrorsPage";
import Login from "app/modules/Auth/pages/Login";
import Registration from "app/modules/Auth/pages/Registration";
import ForgotPassword from "app/modules/Auth/pages/ForgotPassword";

const GoogleMaterialPage = lazy(() =>
  import("app/modules/GoogleMaterialExamples/GoogleMaterialPage")
);
const ReactBootstrapPage = lazy(() =>
  import("app/modules/ReactBootstrapExamples/ReactBootstrapPage")
);
const ECommercePage = lazy(() =>
  import("app/modules/ECommerce/pages/eCommercePage")
);

export default function MergedRoutes({ ...props }) {
  // useEffect(() => {
  //   console.log('Base page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    // <Suspense>
    <Switch>
      //Auth
      <Redirect from="/auth" exact={true} to="/auth/login" />
      <RouteNest isNotPrivate path="/auth" component={AuthPage}>
        <RouteNest isNotPrivate path="/auth/login" component={Login} />
        <RouteNest isNotPrivate path="/auth/registration" component={Registration} />
        <RouteNest isNotPrivate path="/auth/forgot-password" component={ForgotPassword} />
      </RouteNest>
      //user
      <Redirect exact from="/user" to="/user/home" />
      <RouteNest path="/user" component={User}>
        <RouteNest exact title="Home" path={UserHomePath} component={Home} />
        <RouteNest exact title="Buy" path={UserBuyPath} component={Buy} />
      </RouteNest>
      //Admin
      <RouteNest isAdmin path="/admin" component={Layout}>
        <RouteNest
          title="Dashboard"
          isAdmin
          path={AdminDashboardPath}
          component={DashboardPage}
        />
        <RouteNest
          title="Builder"
          isAdmin
          path={AdminBuilderPath}
          component={BuilderPage}
        />
        <RouteNest
          title="My Page"
          isAdmin
          path={AdminMyPagePath}
          component={MyPage}
        />
      </RouteNest>
      <Route isAdmin path="/google-material" component={GoogleMaterialPage} />
      <Route isAdmin path="/react-bootstrap" component={ReactBootstrapPage} />
      <Route isAdmin path="/e-commerce" component={ECommercePage} />
      <Redirect exact from="/admin" to="/admin/dashboard" />
      <Redirect exact from="/" to="/user/home" />
      <Redirect to="/error/error-v1" />
    </Switch>
    // </Suspense>
  );
}

const UserLayout = (props) => {
  return (
    <>
      <Head {...props} />
      {props.children}
      <Footer />
    </>
  );
};
