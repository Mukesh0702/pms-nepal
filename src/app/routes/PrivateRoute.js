import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router-dom";
import isEmpty from "app/helpers/isEmpty";
import ErrorsPage from "app/modules/ErrorsExamples/ErrorsPage";
import { Logout, AuthPage } from "app/modules/Auth";

const PrivateRoute = ({
  component: Component,
  children,
  title,
  auth,
  render,
  isNotPrivate,
  isAdmin,
  ...rest
}) => {
  // const isAuthenticated = auth.user != null;
  // return !isAuthenticated ? (
  //   /*Render auth page when user at `/auth` and not authorized.*/
  //   <Route>
  //     <AuthPage />
  //   </Route>
  // ) : (
  //   /*Otherwise redirect to root page (`/`)*/
  //   <Redirect from="/auth" to="/" />
  // );
  return (
    <Route
      {...rest}
      render={(props) => {
        // return auth.isAuthenticated === true || isNotPrivate (
        //     <Component {...props} ref={ref=>!isEmpty(rest.childRef) ? rest.childRef.current = ref:null} children={children} />
        // ): (
        //     <Redirect to={{pathname:"/login", state: { auth: false }}} />
        //     // window.open(getBaseAxios()),<Redirect to={{pathname:"/login", state: { auth: false }}} />
        // )
        return (
          <ShowRoutes
            {...props}
            auth={auth}
            isNotPrivate={isNotPrivate}
            children={children}
            Component={Component}
            isAdmin={isAdmin}
            title={title}
            ref={(ref) =>
              !isEmpty(rest.childRef) ? (rest.childRef.current = ref) : null
            }
          />
        );
      }}
    />
  );
};

export const ShowRoutes = ({
  Component,
  children,
  title,
  auth,
  ref,
  isNotPrivate,
  isAdmin,
  isEmployee,
  ...rest
}) => {
  const isAuthenticated = auth.user != null;
  const {
    location: { pathname },
  } = rest;
  const { PUBLIC_URL } = process.env;
  let splittedPathname = pathname.split(PUBLIC_URL);
  let pathExists =
    children && children.find((x) => x.props.path === splittedPathname[0]);
  const user = auth.user;
  useEffect(() => {
    // const {
    //   location: { pathname },
    // } = rest;
    if (!isEmpty(title)) {
      document.title = title;
    } else {
      let title = pathname.split("/");
      if (!isEmpty(title) && title.length > 0 && Array.isArray(title)) {
        title = title[title.length - 1].toUpperCase();
      } else {
        title = "PMS";
      }
      document.title = title;
    }
  }, []);

  if (isEmpty(children) || (!isEmpty(children) && pathExists)) {
    if (isAuthenticated || isNotPrivate) {
      // debugger
      if (isAdmin && user && user.roles.includes(2)) {
        // dispatch(addMessage(error));
        return <Redirect to={{ pathname: "/user" }} />;
      }
      if (isEmployee && user && !user.roles.includes(2)) {
        // dispatch(addMessage(error));
        return <Redirect to="/admin" />;
      }
      if (
        rest.location.pathname === "/auth/login" &&
        user &&
        user.roles.includes(1)
      ) {
        return <Redirect to="/admin/dashboard" />;
      }
      if (
        rest.location.pathname === "/auth/login" &&
        user &&
        user.roles.includes(2)
      ) {
        return <Redirect to="/user/home" />;
      }
      return <Component {...rest} ref={(ref) => ref} children={children} />;
    } else if (isAdmin) return <Redirect to="/auth/login" />;
    else return <Component {...rest} ref={(ref) => ref} children={children} />;
    // if (isAdmin) {
    //   if (!isAuthenticated) {
    //     /*Redirect to `/auth` when user is not authorized*/
    //     return <Redirect to="/auth/login" />;
    //   }
    // } else if (isAuthenticated && rest.location.pathname === "/auth/login") {
    //   return <Redirect to="/admin/dashboard" />;
    // }
    //return <Component {...rest} ref={(ref) => ref} children={children} />;
  }
  return <Redirect to="/error/error-v1" />;
};

PrivateRoute.propTypes = {
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps)(PrivateRoute);
