import { RouteNest } from "app/routes/NestedRoutes";
import Buy from "app/users/Scenes/Buy";
import Home from "app/users/Scenes/Home";

import React, { Suspense } from "react";
// import loadable from 'react-loadable';
// import Loader from '../Components/Common/Loader'
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import { UserBuyPath, UserHomePath } from "./UserPaths";
import { LayoutSplashScreen, ContentRoute } from "_metronic/layout";
import { Logout, AuthPage } from "app/modules/Auth";

function UserLinks() {
  return (
    <>
      <RouteNest title="Home" path={UserHomePath} component={Home} />
      <RouteNest title="Buy" path={UserBuyPath} component={Buy} />
    </>
  );
}

export default UserLinks;
