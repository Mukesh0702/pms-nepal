import React, { Suspense, lazy } from "react";

import { LayoutSplashScreen, ContentRoute } from "_metronic/layout";
import { BuilderPage } from "app/pages/BuilderPage";
import { MyPage } from "app/pages/MyPage";
import { DashboardPage } from "app/pages/DashboardPage";
import Home from "../Scenes/Home";
import Buy from "../Scenes/Buy";
import { UserHomePath, UserBuyPath } from "./UserPaths";

export default function UserPage() {
  // useEffect(() => {
  //   console.log('Base page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/user" to={UserHomePath} />
        }
        <ContentRoute path={UserHomePath} component={Home} />
        <ContentRoute path={UserBuyPath} component={Buy} />
        <Redirect to="/error/error-v1" />
      </Switch>
    </Suspense>
  );
}
