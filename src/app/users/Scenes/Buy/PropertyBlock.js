import React, { Component } from "react";
import { FiBell, FiArrowDown, FiArrowUp } from "react-icons/fi";
import { RiArrowUpDownLine } from "react-icons/ri";
import { IoIosArrowDown } from "react-icons/io";
import { FaRegStar } from "react-icons/fa";

class PropertyBlock extends Component {
  render() {
    return (
      <div className="property-block">
        <div className="property-block__header flex justify-between">
          <div className="property-alert flex">
            <FiBell />
            <span className="pl-xsm pr-xsm ">Property alert</span>
            <label htmlFor="">toggle</label>
          </div>
          <div className="property-sort flex">
            <RiArrowUpDownLine className="mr-xsm flex" />

            <span>Sort by:</span>
            <div className="sort-list flex">
              <span className="sort-list__text">Featured</span>
              <IoIosArrowDown className="drop-icon" />
            </div>
          </div>
        </div>
        <div className="property-block__body">
          <ul>
            <li className="listing-card">
              <div className="listing-card-project">
                <div className="listing-card__topspot">
                  <strong>Featured</strong>
                </div>
                <div className="listing-card__wrapper">
                  <div className="top-card">
                    <div className="listing-card-single-image">
                      <a href="#" rel="noopener" className="wrapper-body-link">
                        <div className="listing-card-image">
                          <img
                            src="https://rimh2.domainstatic.com.au/nDfarAwMnE_UnVyeH7BfQ6sWsLc=/660x440/filters:format(jpeg):quality(80)/2016208331_1_1_200409_043538-w1152-h768"
                            alt="Picture of 4/34 Stockton Street, NELSON BAY NSW 2315"
                            class="wrapper-image"
                          />
                        </div>
                      </a>
                    </div>
                    <div className="listing-card-tag">
                      <span class="listing-card-tag__text">New</span>
                    </div>
                    <div className="listing-card-branding">
                      <div className="employee-image">
                        <div className="listing-card-image">
                          <img
                            src="https://rimh2.domainstatic.com.au/IpARlSnIBJwSt49qG8cbai7zCHU=/90x90/filters:format(jpeg):quality(80)/https://images.domain.com.au/img/3592/contact_770336.jpeg?mod=200526-163505"
                            alt="Dane Queenan"
                            className="wd-100"
                          />
                        </div>
                      </div>
                      <div className="emp-details">
                        <span className="bold">Ram Nepal</span>
                        <span className="display-block">
                          Manager, Balkumari Nabil Bank
                        </span>
                      </div>
                      <div className="comp-logo">
                        <div
                          data-testid="listing-card-lazy-image"
                          class="css-6yavch"
                        >
                          <img
                            src="https://rimh2.domainstatic.com.au/vK_a5qcQERos9tcxXtzw2vaDMSQ=/170x60/filters:format(jpeg):quality(80)/https://images.domain.com.au/img/Agencys/3592/logo_3592.png?date=132234297525062016"
                            alt="Logo for PRDNationwide Port Stephens"
                            class="css-0"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="bottom-card">
                    <div className="bottom-card__row">
                      <div className="listing-card-price-wrapper">
                        <p className="listing-card-price">
                          Auction if not sold prior
                        </p>
                        <FaRegStar />
                      </div>
                    </div>
                    <a href="#" class="address is-two-lines" rel="noopener">
                      <h2 class="address-wrapper">
                        <span className="address-line1">
                          Balkumari, neighbourhood, Lalitpur, Nepal
                        </span>
                        <span className="address-line2">
                          <span itemprop="addressLocality">
                            Lubhu Khola, stream{" "}
                          </span>
                          <span itemprop="addressRegion">Lubhu, Nepal</span>
                          <span itemprop="postalCode">2315</span>
                        </span>
                      </h2>
                    </a>
                    <div className="listing-card-features-wrapper">
                      <div className="property-features">
                        <div className="property-features-wrapper">
                          <span className="property-features-feature">
                            <span className="property-features-text-container">
                              5{" "}
                              <span className="property-features-text">
                                Beds
                              </span>
                            </span>
                            <svg
                              viewBox="0 0 18 18"
                              width="24"
                              height="24"
                              className="domain-icon property-feature__icon css-kry84i"
                              aria-hidden="true"
                            >
                              <path
                                fill="none"
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M3.00002 15.00001v1M15.00002 15.00001v1M6 9h6a4 4 0 0 1 4 4v2H2v-2a4 4 0 0 1 4-4z"
                              ></path>
                              <path
                                d="M5.5 9V7.5a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1V8M9.5 8v-.5a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1V9"
                                fill="none"
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                              ></path>
                              <path
                                d="M15 10V4a1 1 0 0 0-1-1H4a1 1 0 0 0-1 1v6"
                                fill="none"
                                stroke="currentColor"
                                stroke-linejoin="round"
                                stroke-width="2"
                              ></path>
                              <path
                                fill="none"
                                stroke="currentColor"
                                stroke-linejoin="round"
                                d="M1.5 12.5h15"
                              ></path>
                              <path fill="none" d="M0 0h18v18H0z"></path>
                            </svg>
                          </span>
                          <span className="property-features-feature">
                            <span className="property-features-text-container">
                              2{" "}
                              <span className="property-features-text">
                                Baths
                              </span>
                            </span>
                            <svg
                              viewBox="0 0 18 18"
                              width="24"
                              height="24"
                              className="domain-icon property-feature__icon css-kry84i"
                              aria-hidden="true"
                            >
                              <path
                                fill="none"
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M5 15v1M13 15v1"
                              ></path>
                              <circle
                                fill="currentColor"
                                cx="13.5"
                                cy="6.5"
                                r=".5"
                              ></circle>
                              <circle
                                fill="currentColor"
                                cx="11.5"
                                cy="4.5"
                                r=".5"
                              ></circle>
                              <circle
                                fill="currentColor"
                                cx="11.5"
                                cy="6.5"
                                r=".5"
                              ></circle>
                              <circle
                                fill="currentColor"
                                cx="9.5"
                                cy="6.5"
                                r=".5"
                              ></circle>
                              <path
                                d="M3 9h12v2a4 4 0 0 1-4 4H7a4 4 0 0 1-4-4V9zM3 9V5a3 3 0 0 1 3-3"
                                fill="none"
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                              ></path>
                              <path
                                d="M6 4.3V2a1 1 0 0 1 1-1h2.3a.7.7 0 0 1 .5 1.2L7.2 4.8A.7.7 0 0 1 6 4.3zM2 9h14"
                                fill="none"
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                              ></path>
                              <path fill="none" d="M0 0h18v18H0z"></path>
                            </svg>
                          </span>
                          <span className="property-features-feature">
                            <span className="property-features-text-container">
                              2{" "}
                              <span className="property-features-text">
                                Parking
                              </span>
                            </span>
                            <svg
                              viewBox="0 0 18 18"
                              width="24"
                              height="24"
                              className="domain-icon property-feature__icon css-kry84i"
                              aria-hidden="true"
                            >
                              <path
                                fill="none"
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-miterlimit="10"
                                d="M16.5 6.5h-2M3.5 6.5h-2M7.5 11.5h3M3.5 7.5h11"
                              ></path>
                              <path
                                d="M15.7 8.8l-1.6-2.6-.2-.4-.5-1.4A2 2 0 0 0 11.6 3H6.4a2 2 0 0 0-1.9 1.4l-.4 1.4-.2.4-1.6 2.6a2 2 0 0 0-.3 1V13a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V9.9a2 2 0 0 0-.3-1.1z"
                                fill="none"
                                stroke="currentColor"
                                stroke-linejoin="round"
                                stroke-width="2"
                              ></path>
                              <path
                                d="M1.5 10.5H5a.5.5 0 0 1 .5.5v.5a1 1 0 0 1-1 1h-3v-2zM13 10.5h3.5v2h-3a1 1 0 0 1-1-1V11a.5.5 0 0 1 .5-.5z"
                                fill="none"
                                stroke="currentColor"
                                stroke-linejoin="round"
                              ></path>
                              <path
                                fill="currentColor"
                                d="M3 14h3v2a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1v-2zM12 14h3v2a1 1 0 0 1-1 1h-1a1 1 0 0 1-1-1v-2z"
                              ></path>
                            </svg>
                          </span>
                          <span className="property-features-feature">
                            <span className="property-features-text-container">
                              780m²{" "}
                            </span>
                            <svg
                              viewBox="0 0 18 18"
                              width="24"
                              height="24"
                              className="domain-icon property-feature__icon css-kry84i"
                              aria-hidden="true"
                            >
                              <line
                                x1="2.5"
                                y1="3"
                                x2="2.5"
                                y2="11"
                                fill="none"
                                stroke="currentColor"
                              ></line>
                              <path
                                fill="currentColor"
                                d="M1.3,3H3.7c.2,0,.4-.2.2-.4L2.7,1.1a.3.3,0,0,0-.5,0L1.1,2.6C.9,2.8,1.1,3,1.3,3Z"
                              ></path>
                              <path
                                fill="currentColor"
                                d="M3.7,11H1.3c-.2,0-.4.2-.2.4l1.2,1.5a.3.3,0,0,0,.5,0l1.2-1.5C4.1,11.2,3.9,11,3.7,11Z"
                              ></path>
                              <line
                                x1="16"
                                y1="15.5"
                                x2="7"
                                y2="15.5"
                                fill="none"
                                stroke="currentColor"
                              ></line>
                              <path
                                fill="currentColor"
                                d="M15,14.3v2.4c0,.2.2.4.4.2l1.5-1.2a.3.3,0,0,0,0-.5l-1.5-1.2C15.2,13.9,15,14.1,15,14.3Z"
                              ></path>
                              <path
                                fill="currentColor"
                                d="M7,16.7V14.3c0-.2-.2-.4-.4-.2L5.1,15.3a.3.3,0,0,0,0,.5l1.5,1.2C6.8,17.1,7,16.9,7,16.7Z"
                              ></path>
                              <rect
                                x="6"
                                y="2"
                                width="10"
                                height="10"
                                fill="none"
                                stroke="currentColor"
                                stroke-width="2"
                              ></rect>
                              <rect width="18" height="18" fill="none"></rect>
                            </svg>
                          </span>
                        </div>
                      </div>
                      <div class="land-type">
                        <span className="land-type__text">House</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default PropertyBlock;
