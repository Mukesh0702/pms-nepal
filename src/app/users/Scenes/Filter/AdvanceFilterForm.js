import React, { Component } from "react";
import classNames from "classnames";
import AutoSelect from "app/users/Components/Common/AutoSelect";
import CustomizedSlider from "app/users/Components/Common/Slider";
import CheckGroup from "app/users/Components/Form/CheckGroup";
import { IoIosArrowDown, IoIosArrowUp } from "react-icons/io";
import SearchButton from "app/users/Components/Common/SearchButton";

export default class AdvanceFilterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      propertyType: [
        { Id: 1, Name: "All", Checked: true },
        { Id: 2, Name: "House", Checked: false },
        { Id: 3, Name: "Apartment", Checked: false },
        { Id: 4, Name: "Townhouse", Checked: false },
        { Id: 5, Name: "Land", Checked: false },
      ],
      formState: {
        startPrice: { label: "Any", value: 0 },
        endPrice: { label: "Any", value: 0 },
        bedRooms: [2, 3],
        bathRooms: [2, 3],
        parkings: [1, 2],
        newEstProperty: { label: "Any", value: 0 },
      },
      showMoreFilter: false,
    };
  }

  handleAutoSelect = (name, selected) => {
    this.setState((prevState) => ({
      ...prevState,
      formState: {
        ...prevState.formState,
        [name]: selected,
      },
    }));
  };
  onSilderChange = (name, newValue) => {
    this.setState((prevState) => ({
      ...prevState,
      formState: {
        ...prevState.formState,
        [name]: newValue,
      },
    }));
  };

  handleCheckBoxChange = (name) => (event) => {
    let value = event.target.checked ? [0, 5] : [2, 3];
    this.setState((prevState) => ({
      ...prevState,
      formState: {
        ...prevState.formState,
        [name]: value,
      },
    }));
  };
  handleMoreFilterShow = (show) => {
    this.setState({ showMoreFilter: show });
  };
  render() {
    const { propertyType, showMoreFilter } = this.state;
    const {
      startPrice,
      endPrice,
      bedRooms,
      parkings,
      bathRooms,
      newEstProperty,
    } = this.state.formState;
    const options = [
      { label: "Any", value: 0 },
      { label: "50,000", value: 1 },
      { label: "1,00,000", value: 2 },
      { label: "1,50,000", value: 3 },
    ];
    const marks = [
      {
        value: 0,
        label: "0",
      },
      {
        value: 1,
        label: "1",
      },
      {
        value: 2,
        label: "2",
      },
      {
        value: 3,
        label: "3",
      },
      {
        value: 4,
        label: "4",
      },
      {
        value: 5,
        label: "5+",
      },
    ];
    const newEstOptions = [
      { value: 0, label: "Any" },
      { value: 1, label: "New Constructions" },
      { value: 2, label: "Established Properties" },
    ];
    return (
      <div className="advance-filter-form">
        <form action="" className="dynamic-search-filters">
          <div className="form-row">
            <div className="form-row__header">
              <span className="header-text">Property Types</span>
              <span className="clear-all">Clear all</span>
            </div>
            <div className="form-row__body">
              {propertyType.map((x) => {
                return (
                  <div
                    className={classNames({
                      "property-type": true,
                      checked: x.Checked,
                    })}
                  >
                    {x.Name}
                  </div>
                );
              })}
            </div>
          </div>

          <div className="form-row">
            <div className="form-row__header">
              <span className="header-text">Price</span>
            </div>
            <div className="form-row__body">
              <AutoSelect
                onChange={this.handleAutoSelect}
                name="startPrice"
                value={startPrice}
                options={options}
                width="150px"
                placeholder="Select"
                isClearable={false}
                //disabled={isViewMode}
              />
              <span className="pr-sm">-</span>
              <AutoSelect
                onChange={this.handleAutoSelect}
                name="endPrice"
                value={endPrice}
                options={options}
                width="150px"
                placeholder="Select"
                isClearable={false}
                //disabled={isViewMode}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-row__header">
              <span className="header-text">Bedrooms</span>
            </div>
            <div className="form-row__body">
              <div>
                <CheckGroup
                  name="anyBedRoom"
                  value={bedRooms}
                  label="Any Bedrooms"
                  checked={bedRooms[1] - bedRooms[0] === 5}
                  onChange={this.handleCheckBoxChange("bedRooms")}
                  labelPosition={"right"}
                />
                <CustomizedSlider
                  step={1}
                  marks={marks}
                  defaultValue={bedRooms}
                  value={bedRooms}
                  valueLabelDisplay={true}
                  min={0}
                  max={5}
                  name="bedRooms"
                  onChange={this.onSilderChange}
                />
              </div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-row__header">
              <span className="header-text">Bathrooms</span>
            </div>
            <div className="form-row__body">
              <div>
                <CheckGroup
                  name="anyBathRoom"
                  value={bathRooms}
                  label="Any BathRooms"
                  checked={bathRooms[1] - bathRooms[0] === 5}
                  onChange={this.handleCheckBoxChange("bathRooms")}
                  labelPosition={"right"}
                />
                <CustomizedSlider
                  step={1}
                  marks={marks}
                  defaultValue={bathRooms}
                  value={bathRooms}
                  valueLabelDisplay={true}
                  min={0}
                  max={5}
                  name="bathRooms"
                  onChange={this.onSilderChange}
                />
              </div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-row__header">
              <span className="header-text">Parking</span>
            </div>
            <div className="form-row__body">
              <div>
                <CheckGroup
                  name="anyParkings"
                  value={parkings}
                  label="Any Parkings"
                  checked={parkings[1] - parkings[0] === 5}
                  onChange={this.handleCheckBoxChange("parkings")}
                  labelPosition={"right"}
                />
                <CustomizedSlider
                  step={1}
                  marks={marks}
                  defaultValue={parkings}
                  value={parkings}
                  valueLabelDisplay={true}
                  min={0}
                  max={5}
                  name="parkings"
                  onChange={this.onSilderChange}
                />
              </div>
            </div>
          </div>

          {showMoreFilter ? (
            <div className="more-filter-items">
              <div className="form-row">
                <div className="form-row__header">
                  <span className="header-text">Land Size</span>
                </div>
                <div className="form-row__body">
                  <div className="fromLandSize">
                    <input type="text" placeholder="eg. 100" />
                    <span className="land-msquared">
                      m<sup class="land-square">2</sup>
                    </span>
                  </div>
                  <span className="pl-sm pr-sm">-</span>
                  <div className="toLandSize">
                    <input type="text" placeholder="eg. 500" />
                    <span className="land-msquared">
                      m<sup class="land-square">2</sup>
                    </span>
                  </div>
                </div>
              </div>
              <div className="form-row">
                <div className="form-row__header">
                  <span className="header-text">New/Established</span>
                </div>
                <div className="form-row__body">
                  <AutoSelect
                    onChange={this.handleAutoSelect}
                    name="newEstProperty"
                    value={newEstProperty}
                    options={newEstOptions}
                    width="200px"
                    placeholder="Select"
                    isClearable={false}
                    //disabled={isViewMode}
                  />
                </div>
              </div>
              <div
                className="less-filter flex"
                onClick={() => this.handleMoreFilterShow(false)}
              >
                <span>Less Filters</span>
                <IoIosArrowUp className="drop-icon" />
              </div>
            </div>
          ) : (
            <div
              className="more-filter flex"
              onClick={() => this.handleMoreFilterShow(true)}
            >
              <span>More Filters</span>
              <IoIosArrowDown className="drop-icon" />
            </div>
          )}

          {/* <div className="form-footer">
            <SearchButton />
          </div> */}
        </form>
      </div>
    );
  }
}
