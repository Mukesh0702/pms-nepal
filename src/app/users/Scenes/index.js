import React from "react";

import isEmpty from "app/helpers/isEmpty";
import Footer from "app/users/Layouts/Footer";
import Head from "app/users/Layouts/NavBar";

const MyContext = React.createContext();
export class User extends React.Component {
  render() {
    const { match, location } = this.props;

    return (
      <div className="user">
        <Head {...this.props} />
        <MyContext.Provider>{this.props.children}</MyContext.Provider>
        <Footer />
      </div>
    );
  }
}
export default User;
