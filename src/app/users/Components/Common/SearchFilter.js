import React, { Component } from "react";
import { RiSearch2Line } from "react-icons/ri";
import isEmpty from "app/helpers/isEmpty";
import AdvanceFilter from "app/users/Scenes/Filter/AdvanceFilter";
import { MdClose } from "react-icons/md";
import { withRouter } from "react-router-dom";
import { UserBuyPath } from "../../Links/UserPaths";

class SearchFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchList: [
        { Id: 1, Name: "Kathmandu" },
        { Id: 2, Name: "Pokhara" },
        { Id: 3, Name: "Dharan" },
        { Id: 4, Name: "Itahari" },
        { Id: 5, Name: "Biratnagar" },
        { Id: 6, Name: "Butwal" },
        { Id: 7, Name: "Dhading" },
        { Id: 8, Name: "Nepalgunj" },
        { Id: 9, Name: "Chitwan" },
        { Id: 10, Name: "Dhulikhel" },
        { Id: 11, Name: "Kalinchowk" },
      ],
      filteredSearchList: [],
      searchFilter: "",
      showFilterList: false,
      searching: false,
      isAdvanceFilterShow: false,
      selectedSearchList: [],
    };
  }

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  handleClickOutside = (event) => {
    let el = document.getElementsByClassName("search-list")[0];
    if (el && !el.contains(event.target)) {
      this.setState({ showFilterList: false });
    }
  };
  componentDidUpdate() {
    let el = document.getElementsByClassName("search-list")[0];
    if (!isEmpty(el)) {
      let maxHeight = el.scrollHeight > 300 ? 300 : el.scrollHeight;
      el.style.bottom = "-" + maxHeight + "px";
      el.style["max-height"] = maxHeight + "px";
    }
  }
  handleSearchInputChange = (event) => {
    this.setState({ searching: true });
    let filteredSearchList = this.getFilteredList(event.target.value);
    this.setState({
      searchFilter: event.target.value,
      showFilterList: true,
      filteredSearchList,
    });
  };
  handleSelectFilter = (filterVal) => {
    const { selectedSearchList } = this.state;
    selectedSearchList.push(filterVal);
    this.setState({
      showFilterList: false,
      searchFilter: "",
      selectedSearchList,
    });
  };

  getFilteredList = (searchFilter) => {
    const { searchList, showFilterList, selectedSearchList } = this.state;

    let filteredSearchList = [];
    setTimeout(() => {
      this.setState({ searching: false });
    }, 1000);
    filteredSearchList = [];
    searchList.map((x) => {
      if (
        !selectedSearchList.find((i) => i.Id === x.Id) &&
        x.Name.toLowerCase().includes(searchFilter.toLowerCase())
      )
        filteredSearchList.push(x);
    });

    // .filter((x) =>
    //   x.Name.toLowerCase().includes(searchFilter.toLowerCase())
    // );
    return filteredSearchList;
  };

  handleAdvanceFilterShow = (show) => {
    document.body.style.overflow = show ? "hidden" : "scroll";
    this.setState({ isAdvanceFilterShow: show });
  };
  handleRemoveSelectedSearch = (id) => {
    let { selectedSearchList } = this.state;
    selectedSearchList = selectedSearchList.filter((x) => x.Id !== id);
    this.setState({ selectedSearchList });
  };
  render() {
    const {
      searchList,
      searchFilter,
      showFilterList,
      searching,
      filteredSearchList,
      isAdvanceFilterShow,
      selectedSearchList,
    } = this.state;
    return (
      <>
        <div className="search-filter">
          <div className="search-filter__input">
            <div className="search-input">
              {!isEmpty(selectedSearchList) && (
                <div className="search-input__selected flex">
                  {selectedSearchList.map((x) => (
                    <div className="selected-list">
                      <span className="selected-list__text">{x.Name}</span>
                      <div
                        className="remove-icon"
                        onClick={() => this.handleRemoveSelectedSearch(x.Id)}
                      >
                        <MdClose />
                      </div>
                    </div>
                  ))}
                </div>
              )}
              <input
                type="text"
                className="search-input__text"
                placeholder={
                  isEmpty(selectedSearchList)
                    ? "Try a location to begin your search"
                    : ""
                }
                onChange={this.handleSearchInputChange}
                value={searchFilter}
              />
            </div>
            {showFilterList && (
              <div className="search-list">
                {searching && <span>Searching ...</span>}
                {filteredSearchList.map((x) => (
                  <span onClick={() => this.handleSelectFilter(x)}>
                    {x.Name}
                  </span>
                ))}
              </div>
            )}

            <button
              type="button"
              className="filter-button button with-icon button-white"
              onClick={() => this.handleAdvanceFilterShow(true)}
            >
              <svg
                viewBox="0 0 24 24"
                aria-hidden="true"
                class="domain-icon css-jeyium"
              >
                <path
                  fill="none"
                  stroke="currentColor"
                  stroke-width="2"
                  d="M3 19h2m6 0h10"
                ></path>
                <circle
                  cx="8"
                  cy="19"
                  r="2.5"
                  fill="none"
                  stroke="currentColor"
                ></circle>
                <path
                  fill="none"
                  stroke="currentColor"
                  stroke-width="2"
                  d="M3 5h2m6 0h10"
                ></path>
                <circle
                  cx="8"
                  cy="5"
                  r="2.5"
                  fill="none"
                  stroke="currentColor"
                ></circle>
                <path
                  fill="none"
                  stroke="currentColor"
                  stroke-width="2"
                  d="M21 12h-2m-6 0H3"
                ></path>
                <circle
                  cx="16"
                  cy="12"
                  r="2.5"
                  fill="none"
                  stroke="currentColor"
                ></circle>
              </svg>
              Filters
            </button>
          </div>

          <button
            type="button"
            className="search-button button with-icon button-primary"
            onClick={() => this.props.history.push(UserBuyPath)}
          >
            <RiSearch2Line />
            <span>Search</span>
          </button>
        </div>
        {isAdvanceFilterShow && (
          <AdvanceFilter
            handleAdvanceFilterShow={this.handleAdvanceFilterShow}
          />
        )}
      </>
    );
  }
}

export default withRouter(SearchFilter);
