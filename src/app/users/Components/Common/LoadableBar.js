import React, { useState } from "react";
import LoadingBar from "react-top-loading-bar";

function LoadableBar() {
  const [loadingBarProgress, setLoadingBarProgress] = useState(50);
  const onLoaderFinished = () => {
    setLoadingBarProgress(0);
  };
  setTimeout(() => {
    setLoadingBarProgress(80);
  }, 300);
  return (
    <LoadingBar
      progress={loadingBarProgress}
      height={3}
      color="blue"
      onLoaderFinished={() => onLoaderFinished()}
    />
  );
}

export default LoadableBar;
