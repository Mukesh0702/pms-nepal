import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { FcHome } from "react-icons/fc";
import NextArrow from "./NextArrow";
import PrevArrow from "./PrevArrow";

export default class BuyBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buyProperty: [
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
      ],
    };
  }

  render() {
    var settings = {
      dots: true,
      infinite: false,
      speed: 1000,
      slidesToShow: 3,
      slidesToScroll: 3,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />,
      useTransform: true,
    };
    return (
      <div className="buy-block">
        <h2 className="buy-block__header">
          Buy for under 1,50,000 in City Kathmandu
        </h2>
        <Slider {...settings}>
          {this.state.buyProperty.map((x) => {
            return (
              <div className="buy-block__block">
                <div className="inner-block">
                  <span className="name">{x.Name}</span>

                  <div className="inner-block__property">
                    <FcHome className="home-icon" />
                    <span className="inner-block__prop-count">
                      {x.PropertyCount} properties for sale
                    </span>
                  </div>
                  <span className="link">View properties</span>
                </div>
              </div>
            );
          })}
        </Slider>
      </div>
    );
  }
}
