import React from "react";
import { RiSearch2Line } from "react-icons/ri";

function SearchButton(props) {
  return (
    <button
      type="button"
      className="search-button button with-icon button-primary"
    >
      <RiSearch2Line className="search-button__icon" />
      <span className="search-button__text">Search</span>
    </button>
  );
}

export default SearchButton;
